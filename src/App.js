import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            refresh: true,
            text: "Show"
        }
        this.onToggleDisplayList = this.onToggleDisplayList.bind(this);
        this.onRefresh = this.onRefresh.bind(this);
    }
    onToggleDisplayList() {
        this.setState({
            text: this.state.text === "Show" ? "Hide" : "Show"
        })
    }
    onRefresh() {
        this.setState({
            text: "Hide"
        })
    }
    render() {
        let todos;
        if (this.state.text === "Show") {
            todos = null;
        } else {
            todos = <TodoList />
        }

        return (
            <div className='App'>
                <button className="displayButton" onClick={this.onToggleDisplayList}>{this.state.text}</button>
                <button className="displayButton" onClick={this.onRefresh} >Refresh</button>
                {todos}
            </div>
        );
    }

}

export default App;
