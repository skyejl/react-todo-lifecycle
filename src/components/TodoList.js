import React, {Component} from 'react';
import './todolist.less';
import TodoItem from "./TodoItem";

class TodoList extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            list: [],
            index: 1
        }
        this.onAddListItem = this.onAddListItem.bind(this);
        console.log("constructor");
    }

    onAddListItem() {
        this.setState({
            list: [...this.state.list, this.state.index++],
            index: this.state.index++
        });
    }

    render() {
        console.log("render");
        return (
            <div className="todoListComponent">
                <button className="addButton" onClick={this.onAddListItem}>Add</button>
                <TodoItem list={this.state.list}/>
            </div>
        );
    }

    componentDidMount() {
        console.log("componentDidMount");
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("componentDidUpdate");
    }

    componentWillUnmount() {
        console.log("componentWillUnmount");
    }
}

export default TodoList;

