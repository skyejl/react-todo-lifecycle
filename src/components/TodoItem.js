import React from 'react';
function TodoItem(props) {
    const listNumber = props.list;
    const listItem = listNumber.map((number) => <li className="Item" key={number}>List Tittle {number}</li>)
    return (
        <ul className="listItem">{listItem}</ul>
    );
}
export default TodoItem;
